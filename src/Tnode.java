/*Kasutatud kirjandus
 * http://stackoverflow.com/questions/21716830/java-using-a-stack-to-reverse-the-words-in-a-sentence
 * http://stackoverflow.com/questions/19895308/converting-from-reverse-polish-notationrpn-into-a-tree-form
 * https://www.cs.colostate.edu/~cs165/.Spring17/assignments/P7/files/AbstractTree.java
 * http://stackoverflow.com/questions/237159/whats-the-best-way-to-check-to-see-if-a-string-represents-an-integer-in-java
 * Mõningal määral ka kaastudengid
*/
import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;
   
   public Tnode() 
	{
	   
	}

	public Tnode(String s) 
	{
		name = s;}
	
	 public void setNextSibling (Tnode p) {
	      nextSibling = p;
	   }  

	   public void setFirstChild (Tnode a) {
	      firstChild = a;
	   }
	   

   @Override
   public String toString() {
	   StringBuffer b = new StringBuffer();
		b.append(this.name);
		if (this.firstChild != null) 
		{
			b.append("(");
			b.append(this.firstChild.toString());
		}
		if (this.nextSibling != null) 
		{
			b.append(",");
			b.append(this.nextSibling.toString());
			b.append(")");
		}
      // TODO!!!
      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
	   if (pol.trim().isEmpty()) throw new RuntimeException("Entered string was empty!" + pol);

	      String[] split = pol.trim().split("\\s+");
	      int numbers = 0;
	      int operators = 0;

	      Stack<Tnode> tnodeStack = new Stack<>();

	      for (String s : split) {
	         Tnode tnode = new Tnode(s);

	         if (doubleCheck(s)){
	            tnodeStack.push(tnode);
	            numbers++;
	         }else {
	            if(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {
	               if(tnodeStack.size() < 2)
	                  throw new RuntimeException("Fewer than 2 elements in list when trying " +s+ " from expression " +pol + "´while trying to convert to tree!");

	               Tnode temp = tnodeStack.pop();
	               Tnode temp2 = tnodeStack.pop();

	               temp2.setNextSibling(temp); //viitab teisena pop-itule esimene pop-itu.
	               tnode.setFirstChild(temp2); //viitab käesolevale tnode-le firstchild-i.

	               tnodeStack.push(tnode);
	               operators ++;
	            }else{
	               throw new RuntimeException("Only operators +,-,*,/ expected, operator " +s+ " was used instead in expression " +pol);
	            }
	            

	         }
	         if (tnodeStack.size()==0){
	  		   throw new RuntimeException(" Operator does not have enough arguments! Your entry: " + pol);
	  	   }
	      }

	      if(!((operators+1) == numbers)) {
	         throw new RuntimeException("Numbers count and operators count is not in balance when interpreting expression: " +pol);
	      }

	      return tnodeStack.pop();
      // TODO!!!
   }
   
   public static boolean doubleCheck(String s) {
	      try {
	         Double.parseDouble(s);
	      } catch(NumberFormatException e) {
	         return false;
	      }
	      return true;
	   }

   public static void main (String[] param) {
	      //String rpn = "1 2 +";
	      //String rpn = "5 1 - 7 * 6 3 / +";
	      //String rpn = "       5        1 -         7        *   6       3 / +          ";
	      //String rpn = "";
	      //String rpn ="2 +";
	      //String rpn = "g";
	      String rpn = "5 1 - 7 * 6 4 3 / + ";
	      //String rpn = "1";
	      System.out.println ("RPN: " + rpn);
	      Tnode res = buildFromRPN (rpn);
	      System.out.println ("Tree: " + res.toString());
      // TODO!!! Your tests here
   }
}

